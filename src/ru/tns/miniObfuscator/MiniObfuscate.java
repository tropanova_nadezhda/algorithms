package ru.tns.miniObfuscator;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Класс,реализующий мини-обфускатор
 *
 * @author Tropanova N.S.
 */
public class MiniObfuscate {

    public static void main(String[] args) throws IOException {
        String fileWay = "src\\ru\\tns\\miniObfuscator\\Main.java";

        String listLine = fileReader(fileWay);
        printOut(listLine);
        String multiLine = deleteComments(removesSpaces(listLine));

        String fileName = getFileName(fileWay);
        String renameMultiline = replaceFileName(multiLine, fileName);
        String newFileFullWay = newFileFullWay(fileWay, fileName);
        fileRecorder(renameMultiline, newFileFullWay);

    }

    /**
     * Считывает файл по указанному пути
     *
     * @param fileWay путь к файлу
     * @return файл в мультистроке
     */
    private static String fileReader(String fileWay) {
        StringBuilder listLine = new StringBuilder();
        try {
            List<String> list = Files.readAllLines(Paths.get(fileWay));
            //получение мультистроки
            for (String s : list) {
                listLine.append(s); //listLine += s
            }
        } catch (IOException e) {
            System.out.println("not found");
        }
        return listLine.toString();
    }

    /**
     * Удаляет комментарии
     *
     * @param listLine файл в мультистроке
     * @return мультистроку без комментариев
     */
    private static String deleteComments(String listLine) {
        return listLine.replaceAll("(/\\*.+?\\*/)|(//.+?)[:;a-zA-Zа-яА-ЯЁё]*", "");
    }

    /**
     * Меняет имя класса в файле
     *
     * @param multiLine мультистрока без комментариев
     * @param fileName  имя файла
     * @return
     */
    private static String replaceFileName(String multiLine, String fileName) {
        String fileNewName = "New" + fileName;
        return multiLine.replaceAll(fileName, fileNewName);
    }

    /**
     * Удаляет лишние пробелы
     *
     * @param listLine мультистрока без комментариев
     * @return очищенную мультистроку
     */
    private static String removesSpaces(String listLine) {
        return listLine.replaceAll("\\s+(?![^\\d\\s])", "");
    }

    /**
     * Получает имя из пути файла без расширения
     *
     * @param fileWay путь файла
     * @return файл без расширения
     */
    private static String getFileName(String fileWay) {
        Path p = Paths.get(fileWay);
        String fileName = p.getFileName().toString();
        String fileNameWithoutExtension = fileName.replaceAll("\\..*", "");
        return fileNameWithoutExtension;
    }

    /**
     * Меняет имя файла в пути
     *
     * @param fileWay  путь к файлу
     * @param fileName имя файла
     * @return путь с новым именем файла
     */
    private static String newFileFullWay(String fileWay, String fileName) {
        return fileWay.replaceAll(fileName, "\\New" + fileName);
    }

    /**
     * Записывает мультистроку в файл
     *
     * @param renameMultiline мультистрока с измененным Main
     * @param newFileFullWay  путь к файлу с новым именем
     * @throws IOException ошибку
     */
    private static void fileRecorder(String renameMultiline, String newFileFullWay) throws IOException {
        try (FileWriter writer = new FileWriter(newFileFullWay)) {
            writer.write(renameMultiline);
        }
    }

    /**
     * Выводит мультистроку в консоль
     *
     * @param listLine мультистрока
     */
    private static void printOut(String listLine) {
        System.out.println(listLine);
    }
}