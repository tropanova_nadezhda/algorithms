package ru.tns.miniObfuscator;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = {5, 8, 1, 3, 9, 6, 2, 7, 4};
        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(3, 9, 6, 2, 7, 4, 8, 1, 5));
        System.out.println("До сортировки Array - " + Arrays.toString(array));
        insertSort(array);
        System.out.println("После сортировки Array - " + Arrays.toString(array) + "\n");

        System.out.println("До сортировки ArrayList - " + arrayList);
        insertSort(arrayList);
        System.out.println("После сортировки ArrayList - " + arrayList);
    }

    //sdfhsdfkj
    private static void insertSort(int[] array) {
        for (int out = 1; out < array.length; out++) {
            int temp = array[out];
            int in = out;
            while (in > 0 && array[in - 1] >= temp) {
                array[in] = array[in - 1];
                in--;
            }
            array[in] = temp;
        }
    }

    //djfhhjsdjisdjkhfbsdfjkbhsvg
    private static void insertSort(ArrayList<Integer> arrayList) {

        for (int out = 1; out < arrayList.size(); out++) {
            int temp = arrayList.get(out);
            int in = out;
            while (in > 0 && arrayList.get(in - 1) <= temp) {
                arrayList.set(in, arrayList.get(in - 1));
                in--;
            }
            arrayList.set(in, temp);
        }
    }
}

