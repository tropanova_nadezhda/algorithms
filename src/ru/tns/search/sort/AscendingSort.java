package ru.tns.search.sort;

import java.util.Arrays;

/*
 * Класс создан для использования сортировки выбором по возрастанию
 *
 * @autor Tropanova N.S
 */
public class AscendingSort {
    public static void main(String[] args) {
        int[] arr = {4, 2, 6, 1, 7, 9, 3, 5, 8};
        System.out.println("Массив чисел до сортировки выбором: " + Arrays.toString(arr));
        selectionSort(arr);
        System.out.println("Массив чисел после сортировки выбором: " + Arrays.toString(arr));
    }

    /**
     * Сортирует массив целых чисел по возрастанию с помощью сортировки выбором
     *
     * @param arr массив чисел
     */
    private static void selectionSort(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            int min = arr[i];
            int minI = i;

            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < min) {
                    min = arr[j];
                    minI = j;
                }
            }

            if (i != minI) {
                int tmp = arr[i];
                arr[i] = arr[minI];
                arr[minI] = tmp;
            }
        }
    }
}










