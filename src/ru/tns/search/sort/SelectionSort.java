package ru.tns.search.sort;
/*
 * Класс создан для использования сортировки выбором по убыванию в списке ArrayList
 *
 * @autor Tropanova N.S
 */

import java.util.ArrayList;

public class SelectionSort {
    public static void main(String[] args) {
        ArrayList<Integer> arr = new ArrayList<>();
        arr.add(9);
        arr.add(4);
        arr.add(3);
        arr.add(5);
        arr.add(7);
        arr.add(8);
        arr.add(1);
        arr.add(6);
        arr.add(2);
        System.out.println("Массив чисел до сортировки выбором: " + arr);
        sort(arr);
        System.out.println("Массив чисел после сортировки выбором: " + arr);
    }
    /**
     * Упорядочивает числовую последовательность по убыванию с помощью сортировки выбором
     * @param arr массив чисел
     */
    private static void sort(ArrayList<Integer> arr){
        int size = arr.size();
        for (int i = 0; i < size; i++) {
              /*Предполагаем, что первый элемент (в каждом
           подмножестве элементов) является минимальным */
            int min = arr.get(i);
            int minI = i;

            for (int j = i + 1; j < size; j++) {
                //Если находим, запоминаем его индекс
                if (arr.get(j) > min) {
                    min = arr.get(j);
                    minI = j;
                }
            }
            /*
             * Если нашелся элемент, меньший, чем на текущей позиции,
             *           меняем их местами
             */
            if (i != minI) {
                int tmp = arr.get(i);
                arr.set(i, arr.get(minI));
                arr.set(minI, tmp);
            }
        }
    }
}




