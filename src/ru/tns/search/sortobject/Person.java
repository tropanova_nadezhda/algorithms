package ru.tns.search.sortobject;

public class Person {
    private String surname; // фамилия
    private int yearOfBirth; // год рождения

    Person(String surname, int yearOfBirth) {
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
    }

    String getSurname() {
        return surname;
    }

    int getYearOfBirth() {
        return yearOfBirth;
    }

    public String toString() { // метод toString(), позволяющий сформировать строку с характеристиками ...
        return "Patient{" +
                "surname: " + surname +
                "; year of birth: " + yearOfBirth +
                '}';
    }
}



