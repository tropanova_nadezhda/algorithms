package ru.tns.search.sortobject;

/*
 * Класс создан для использования сортировки объектов:
 * Сортировка в алфавитном порядке
 * Сортировка по годам рождения (по убыванию)
 * Поиск по фамилии
 *
 * @author Tropanova N.S.
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Person person1 = new Person("Рожков", 1854);
        Person person2 = new Person("Кошкин", 1932);
        Person person3 = new Person("Власов", 1978);
        Person person4 = new Person("Смирнов", 2009);
        Person person5 = new Person("Соколов", 1929);


        ArrayList<Person> persons = new ArrayList<>();
        persons.add(person1);
        persons.add(person2);
        persons.add(person3);
        persons.add(person4);
        persons.add(person5);
        System.out.println(persons);

        System.out.println(sortSurname(persons));
        System.out.println((yearOfBirth(persons)));
        System.out.println(checksSurname(persons));
    }

    /**
     * Метод сортирует массив объектов(@code persons) в алфавитном порядке с помощью сортировки выбором
     * Если попадаются одинаковые фамилии, то этот массив объектов сортируется по годам рождения (по возрастанию)
     *
     * @param persons массив объектов
     * @return возвращает отсортированный массив объектов
     */
    private static ArrayList<Person> sortSurname(ArrayList<Person> persons) {
        Person temp;
        for (int i = 0; i < persons.size(); i++) {
            for (int j = 1 + i; j < persons.size(); j++) {
                int compare = persons.get(j).getSurname().compareTo(persons.get(i).getSurname());
                if (compare < 0) {
                    temp = persons.get(i);
                    persons.set(i, persons.get(j));
                    persons.set(j, temp);
                }

                if (compare == 0) {
                    if (persons.get(j).getYearOfBirth() < persons.get(i).getYearOfBirth()) {
                        temp = persons.get(i);
                        persons.set(i, persons.get(j));
                        persons.set(j, temp);
                    }
                }
            }
        }
        return persons;
    }

    /**
     * Метод сортирует массив объектов (@code persons) по годам рождения с помощью сортировки выбором (по убыванию)
     *
     * @param persons массив объектов
     * @return возращает отсортированный массив
     */
    private static ArrayList<Person> yearOfBirth(ArrayList<Person> persons) {
        Person temp; // вспомогательная переменная
        for (int i = 0; i < persons.size(); i++) {
            for (int j = i + 1; j < persons.size(); j++) {
                if (persons.get(j).getYearOfBirth() > persons.get(i).getYearOfBirth()) {
                    temp = persons.get(i);
                    persons.set(i, persons.get(j));
                    persons.set(j, temp);
                }
            }
        }
        return persons;
    }

    /**
     * Метод проверяет массив объектов (@code persons) на наличие введенной пользователем фамилии и выводит данные об объекте
     *
     * @param persons массив объектов
     * @return вывод данных об объекте
     */
    private static int checksSurname(ArrayList<Person> persons) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите фамилию: ");
        String surname = scanner.nextLine();
        for (int i = 0; i < persons.size(); i++) {
            if (persons.get(i).getSurname().equals(surname)) {
                System.out.println("Фамилия " + surname + " найдена по индексу: ");
                return i;
            }
        }
        System.out.println("Фамилия " + surname + " не найдена");
        return 0;
    }
}




