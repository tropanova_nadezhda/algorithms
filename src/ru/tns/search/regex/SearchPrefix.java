package ru.tns.search.regex;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс для реализации поиска слов, начинающихся с приставок при- или пре-
 * входные данные(input.txt) --- программа(SearchPrefix) --- выходные данные(output.txt)
 * @author Tropanova N.S.
 */
public class SearchPrefix {
    public static void main(String[] args) {
        String s;
        Pattern pattern = Pattern.compile("\\b(пре|при|Пре|При)[а-яё]+\\b");
        ArrayList<String> text= new ArrayList<>();
        Matcher matcher = pattern.matcher("");

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\ru\\tns\\search\\regex\\input.txt"))) {
            while ((s = bufferedReader.readLine()) != null) {
                matcher.reset(s);
                while (matcher.find()) {
                   text.add(matcher.group());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        System.out.println(text.toString());
        print(text);
    }

    public static void print(ArrayList <String> text) {
        try (FileWriter writer = new FileWriter("src\\ru\\tns\\search\\regex\\output.txt")) {
            writer.write(String.valueOf(text));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
