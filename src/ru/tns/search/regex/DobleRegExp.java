package ru.tns.search.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс для поиска вещественных чисел с плавающей и фиксированной точкой
 *
 * @author Tropanova N.S.
 */
public class DobleRegExp {
    public static void main(String[] args) {
        String string = "2.5 -5.78 плюс + +67 .8 9. +. 23.12e+10";
        Pattern pattern = Pattern.compile("([+-]?(\\d*\\.\\d+)|(\\d+\\.\\d*))\\W");
        Pattern pattern1 = Pattern.compile("[+-]?\\d*\\.\\d+([eE][-+]\\d+)");

        System.out.println("Числа с фиксированной точкой: ");
        regex(string, pattern);

        System.out.println("Числа с плавующей точкой: ");
        regex(string, pattern1);
    }

    private static void regex(String string, Pattern pattern) {
        Matcher matcher = pattern.matcher(string);
        while (matcher.find()) {
            System.out.println(matcher.group());
        }
    }
}

