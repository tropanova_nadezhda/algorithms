package ru.tns.search.bubble;
/*
 * Класс создан для использования пузырьковой сортировки по убыванию
 *
 * @autor Tropanova N.S.
 */

import java.util.ArrayList;

public class BubbleSortArrayList {

    public static void main(String[] args) {
        ArrayList<Integer> arr = new ArrayList<>();
        arr.add(6);
        arr.add(4);
        arr.add(1);
        arr.add(5);
        arr.add(7);
        arr.add(8);
        arr.add(3);
        arr.add(9);
        arr.add(2);
        System.out.println("Массив чисел пузырьковой сортировки: " + arr); // выводим массив перед пуз.сортировки

        bubbleSort(arr); // сортируем массив
        System.out.println("Массив чисел после пузырьковой сортировки : " + arr); // выводим массив после пуз.сортировки
    }
    /**
     * Упорядочивает числовую последовательность по убыванию с помощью пуз.сортировки
     * @param arr массив чисел
     */
    private static void bubbleSort(ArrayList<Integer> arr) {
        boolean flag = true;
        int temp; // вспомогательная переменная
        while (flag) {
            flag = false;
            int size = arr.size();
            for (int j = 0; j < size - 1; j++) {
                if (arr.get(j) < arr.get(j + 1)) { // изменение элемента на 1
                    temp = arr.get(j); // меняем элементы местами
                    arr.set(j, arr.get(j + 1));
                    arr.set(j + 1, temp);
                    flag = true; // true означает что замена была проведена
                }
            }
        }
    }
}





