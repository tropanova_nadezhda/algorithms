package ru.tns.search.bubble;
/*
 * Класс создан для использования пузырьковой сортировки по возрастанию
 *
 * @autor Tropanova N.S.
 */
import java.util.Arrays;

public class BubbleSort {
    public static void main(String[] args) {
        int[] arr = {4, 2, 6, 1, 7, 9, 3, 5, 8};
        System.out.println(" До сортировки: " + Arrays.toString(arr));
        bubbleSort(arr);
        System.out.println(" После сортировки: " + Arrays.toString(arr));
    }

    /**
     * Сортируем массив целых чисел по возрастанию с помощью пуз.сортировки
     *
     * @param arr массив чисел
     */
    private static void bubbleSort(int[] arr) {
        boolean flag = true;
        int temp;
        while (flag) {
            flag = false;
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                    flag = true;
                }
            }
        }
    }
}




