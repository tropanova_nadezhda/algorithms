package ru.tns.search.algoritm;
/*
 * Класс создан для использования линейного поиска
 *
 * @autor Tropanova N.S.
 */

import java.util.Scanner;

public class ArrayElement {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] number = {3, 2, 5, 1, 6, 11, 74, 104, 13, 32};
        System.out.println("Введите число которое надо найти:");
        int num = scanner.nextInt();
        searchElement(number, num);
    }

    /**
     * Ищет индекс введенного числа
     *
     * @param arr массив
     * @param num введенное число
     */
    private static void searchElement(int[] arr, int num) {
        for (int index = 0; index < arr.length; index++) {
            if (arr[index] == num) {
                System.out.println("Число " + num + " найдено по индексу: " + index);
                return;
            }
        }
        System.out.println("Числа " + num + " нет");
    }
}






