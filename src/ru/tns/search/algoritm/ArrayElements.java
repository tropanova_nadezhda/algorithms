package ru.tns.search.algoritm;
/*
 * Класс создан для использования линейного поиска + ArrayList
 *
 * @autor Tropanova N.S.
 */

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayElements {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        ArrayList<Integer> number = new ArrayList<>();
        number.add(6);
        number.add(9);
        number.add(11);
        number.add(252);
        number.add(15);
        number.add(8);
        number.add(13);
        number.add(7);
        number.add(29);
        number.add(1);

        System.out.print("Введите число для поиска: ");
        int num = scanner.nextInt();

        System.out.println("Массив чисел: " + number);
        searchElement(number, num);
    }

    /**
     * Ищет индекс введенного числа
     *
     * @param number массив
     * @param num    введенное число
     */
    private static void searchElement(ArrayList<Integer> number, int num) {
        int size = number.size();
        for (int index = 0; index < size; index++) {
            if (number.get(index) == num) {
                System.out.println("Число " + num + " найдено по индексу: " + index);
                return;
            }
        }
        System.out.println("Числа " + num + " нет");
    }
}
