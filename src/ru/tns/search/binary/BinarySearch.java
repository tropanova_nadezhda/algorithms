package ru.tns.search.binary;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

/*
 * Класс создан для использования бинарного поиска по возрастанию
 *
 * @autor Tropanova N.S.
 */
public class BinarySearch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 10);
        }
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            arrayList.add((int) (Math.random() * 10));
        }
        System.out.print("Введите число для поиска:");
        int number = scanner.nextInt();

        System.out.println("До сортировки: " + Arrays.toString(array));
        Arrays.sort(array);
        System.out.println("После сортировки: " + Arrays.toString(array));

        System.out.println("До сортировки ArrayList - " + arrayList.toString());
        Collections.sort(arrayList);
        System.out.println("После сортировки ArrayList - " + arrayList.toString());

        binarySearch(array, number);
        binarySearch(arrayList, number);

    }

    /**
     * Возвращает индекс искаемого числа в отсортированном массиве (@code array) по возрастанию
     *
     * @param array  массив целых чисел
     * @param number искаемое число в массиве
     */
    private static void binarySearch(int[] array, int number) {

        int first = 0;
        int last = array.length - 1;

        while (first <= last) {
            int mid = (first + last) / 2;

            int temp = array[mid];

            if (temp == number) {
                System.out.println("Число " + number + " найдено по индексу: " + last);
                return;
            }
            if (temp < number) {
                first = mid + 1;

            } else {
                last = mid - 1;
            }
        }
        System.out.println("Числа " + number + " нет");
    }

    /**
     * Возвращает индекс искаемого числа в отсортированном массиве (@code arrayList) по возрастанию
     *
     * @param arrayList массив целых чисел
     * @param number    искаемое число в массиве
     */
    private static void binarySearch(ArrayList<Integer> arrayList, int number) {

        int first = 0;
        int last = arrayList.size() - 1;

        while (first <= last) {
            int mid = (first + last) / 2;

            int temp = arrayList.get(mid);

            if (temp == number) {
                System.out.println("Число " + number + " найдено по индексу: " + last);
                return;
            }
            if (temp < number) {
                first = mid + 1;

            } else {
                last = mid - 1;
            }
        }
        System.out.println("Числа " + number + " нет");
    }
}
