package ru.tns.search.interfaces;

import ru.tns.search.interfaces.printers.IPrinter;
import ru.tns.search.interfaces.readers.IReader;

class Replacer {
    private IReader reader;
    private IPrinter printer;

    Replacer(IReader reader, IPrinter printer) {  // получает и сохраняет у себя в полях
        this.reader = reader;
        this.printer = printer;
    }

    void replace(){
        final String text = reader.read(); // откуда то читает строку
        final String replacedText = text.replaceAll(":\\)", ":3");
        printer.print(replacedText); // выводится куда то
    }
}

