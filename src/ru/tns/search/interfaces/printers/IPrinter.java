package ru.tns.search.interfaces.printers;

// выводит результаты
public interface IPrinter {
    void print(final String text); // не изменяемая строка,потому что final
}

// интерфейс позволяет описать на абстрактном уровне где и что...
