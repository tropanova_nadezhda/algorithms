package ru.tns.search.interfaces.printers;

public class ConsolePrinter implements IPrinter {

    @Override
    public void print(final String text) {
        System.out.println(text);
    }

    @Override
    public void println(final int lengthText) {
        System.out.println(lengthText);


    }
}
