package ru.tns.search.interfaces.printers;

public class AdvConsolePrinter implements IPrinter {


    @Override
    public void print(String text) {
        System.out.println(text);
    }

    @Override
    public void println(int lengthText) {
        System.out.println(lengthText);
    }


}
