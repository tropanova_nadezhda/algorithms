package ru.tns.search.interfaces.readers;
// для предопределённый текста

public interface IReader {
    String read();
    // что бы получить строку надо использовать метод read();
}
