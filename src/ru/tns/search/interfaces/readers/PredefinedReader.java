package ru.tns.search.interfaces.readers;

public class PredefinedReader implements IReader {
    private String text; // берёт

    public PredefinedReader(String text) { // сохраняет
        this.text = text;
    }

    @Override
    public String read() { // возвращаем текст
        return text;
    }
}
