package ru.tns.search.interfaces;
import ru.tns.search.interfaces.printers.AdvConsolePrinter;
import ru.tns.search.interfaces.printers.ConsolePrinter;
import ru.tns.search.interfaces.printers.DecorativePrinter;
import ru.tns.search.interfaces.printers.IPrinter;
import ru.tns.search.interfaces.readers.IReader;
import ru.tns.search.interfaces.readers.PredefinedReader;

public class Main {
    public static void main(String[] args) {

        IReader reader = new PredefinedReader("Привет:) Пока-пока:)");
        IPrinter printer = new ConsolePrinter();
        IPrinter advPrinter = new AdvConsolePrinter();
        IPrinter decPrinter = new DecorativePrinter();

        Replacer replacer = new Replacer(reader, printer);
        Replacer advReplacer = new Replacer(reader, advPrinter);
        Replacer decReplacer = new Replacer(reader, decPrinter);

        replacer.replace();
        advReplacer.replace();
        decReplacer.replace();

    }
}
