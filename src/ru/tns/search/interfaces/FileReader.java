package ru.tns.search.interfaces;

import ru.tns.search.interfaces.readers.IReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileReader implements IReader {

    private String nameFile;

    FileReader(final String nameFile) {
        this.nameFile = nameFile;
    }

    public String read() {
        StringBuilder listLine = new StringBuilder(" ");
        try {
            List<String> list = Files.readAllLines(Paths.get(nameFile));
            int temp = list.size();
            for (int i = 0; i < temp; i++) {
                listLine.append(list.get(i));
            }
        } catch (IOException e) {
            System.out.println("Произошла ошибка");
        }
        return listLine.toString();
    }
}

