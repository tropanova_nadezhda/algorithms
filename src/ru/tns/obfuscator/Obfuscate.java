package ru.tns.obfuscator;

import java.io.IOException;

/**
 * Класс, запускающий процесс обфускации вызовом метода obfuscate()
 *
 * @author Tropanova N.S.
 */
public class Obfuscate {
    public static void main(String[] args) throws IOException {
        FileObfuscatory fileObfuscatory = new FileObfuscatory("D:\\work\\cod.java", "D:\\work\\");
        fileObfuscatory.obfuscate();
    }
}



