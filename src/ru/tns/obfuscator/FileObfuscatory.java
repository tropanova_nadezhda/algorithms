package ru.tns.obfuscator;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Класс, реализующий обфускатор файла. фасад
 *
 * @author Tropanova N.S.
 */
class FileObfuscatory {
    private String soursPath;
    private String targetPath;
    FileObfuscatory(String soursPath, String targetPath) {
        this.soursPath = soursPath;
        this.targetPath = targetPath;
    }

    /**
     * Возвращает полученное имя из пути файла без расширения
     *
     * @param soursPath путь файла
     * @return имя файла без расширения
     */
    private static String getFileName(String soursPath) {
        Path name = Paths.get(soursPath);
        String fileName = name.getFileName().toString();
        return fileName.replaceAll("\\..*", "");
    }

    /**
     * Возвращает измененное имя файла в пути
     *
     * @param soursPath  путь к файлу
     * @param targetPath имя файла
     * @return путь с новым именем файла
     */
    private static String newFileFullWay(String soursPath, String targetPath) {
        return soursPath.replaceAll(targetPath, "\\New" + targetPath);
    }

    /**
     * Записывает мультистроку в файл
     *
     * @param multiLine      обфусцированная мультистрока
     * @param newFileFullWay путь к файлу с новым именем
     * @throws IOException ошибку
     */
    private static void fileRecorder(String multiLine, String newFileFullWay) throws IOException {
        try (FileWriter writer = new FileWriter(newFileFullWay)) {
            writer.write(multiLine);
        }
    }

    /**
     * Выводит мультистроку в консоль
     *
     * @param listLine мультистрока
     */
    private static void printOut(String listLine) {
        System.out.println(listLine);
    }

    /**
     * Выполняет и выводит рез-ты процесса обфускации
     *
     * @throws IOException ошибку
     */
    void obfuscate() throws IOException {
        String listLine = fileReader(soursPath);
        String multiLine = deleteComments(deleteGamps(listLine));

        targetPath = getFileName(soursPath);
        String newFileFullWay = newFileFullWay(soursPath, targetPath);
        fileRecorder(multiLine, newFileFullWay);
        printOut(listLine);
    }

    /**
     * Возвращает считанный из файоа код в виде мультистроки
     *
     * @param soursPath путь к файлу
     * @return считанный из файоа код в виде мультистроки
     */
    private String fileReader(String soursPath) {
        StringBuilder listLine = new StringBuilder();
        try {
            List<String> list = Files.readAllLines(Paths.get(soursPath));
            for (String s : list) {
                listLine.append(s);
            }
        } catch (IOException e) {
            System.out.println("Ошибка");
        }
        return listLine.toString();
    }

    /**
     * Возвращает мультистроку без лишних комментариев
     *
     * @param listLine мультистрока
     * @return мультистроку без лишних комментариев
     */
    private String deleteComments(String listLine) {
        return listLine.replaceAll("(/\\*.+?\\*/)|(//.+?)[:;a-zA-Zа-яА-ЯЁё]*", "");
    }

    /**
     * Возвращает мульистроку без лишних пробелов
     *
     * @param listLine мультистрока без комментариев
     * @return мультистроку без лишних пробелов
     */
    private String deleteGamps(String listLine) {
        return listLine.replaceAll("\\s+(?![^\\d\\s])", " ");
    }
}
