package ru.tns.interfaces.printers;

public class DecoratePrinter implements IPrinter {

    @Override
    public void print(String text) {
        System.out.println("***** " + text + " *****");
    }
}

