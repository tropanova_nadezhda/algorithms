package ru.tns.interfaces.printers;


public class AdvConsolePrinter implements IPrinter {
    @Override
    public void print(final String text) {
        System.out.println(text);
        System.out.println("Количество символов: " + text.length());
    }
}