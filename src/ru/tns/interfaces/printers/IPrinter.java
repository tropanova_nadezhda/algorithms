package ru.tns.interfaces.printers;

public interface IPrinter {
    void print(final String text);
}
