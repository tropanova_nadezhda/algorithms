package ru.tns.interfaces;


import ru.tns.interfaces.printers.AdvConsolePrinter;
import ru.tns.interfaces.printers.IPrinter;
import ru.tns.interfaces.printers.DecoratePrinter;
import ru.tns.interfaces.readers.IReader;
import ru.tns.interfaces.readers.PredefinedReader;
import ru.tns.interfaces.readers.ScannerReader;
import  ru.tns.interfaces.replacer.Replacer;


import ru.tns.interfaces.printers.AdvConsolePrinter;
import ru.tns.interfaces.printers.ConsolePrinter;
import ru.tns.interfaces.printers.DecoratePrinter;
import ru.tns.interfaces.printers.IPrinter;
import ru.tns.interfaces.readers.FileReader;
import ru.tns.interfaces.readers.IReader;
import ru.tns.interfaces.readers.PredefinedReader;
import ru.tns.interfaces.readers.ScannerReader
        ;
import ru.tns.interfaces.replacer.Replacer;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        IReader reader = new PredefinedReader("Привет :) Пока-пока:)");
//        IReader reader = new ScannerReader();
        IReader readerFile = new FileReader();
        IPrinter printer = new ConsolePrinter();
        IPrinter decoratePrint = new DecoratePrinter();
        Replacer replacer = new Replacer(reader, printer);
        Replacer advReplacer = new Replacer(reader, decoratePrint);
        Replacer replacerFile = new Replacer(readerFile, decoratePrint);
        replacer.replace();
        advReplacer.replace();
        replacerFile.replace();

    }
}