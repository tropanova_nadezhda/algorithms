import java.util.Scanner;

public class JavaCode {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n, x, y, z;
        System.out.print("Введите n: ");
        n = scanner.nextInt();
        x = 1;
        while (x < n) {
            y = x;

            while (y <= n) {
                z = y;

                while (z <= n) {

                    if ((z <= x * 2) && (y <= x * 2) && (z * z == x * x + y * y)) {
                        System.out.println(x + " " + y + " " + z);
                        break;
                    }
                    z++;
                }
                y++;
            }
            x++;
        }
    }
}
